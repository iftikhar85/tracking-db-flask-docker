from datetime import datetime


def DebugLogging(Debuglogs):
    TodaysDate = datetime.now().strftime('%Y-%m-%d')
    LogsFileName = 'Logs_' + TodaysDate + '.txt'
    file = open(LogsFileName, 'a')

    Timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    DebugData = str(Timestamp) + ': ' + str(Debuglogs) + '\n'
    file.write(DebugData)
    file.close()
